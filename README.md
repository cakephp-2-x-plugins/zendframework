ZendFramework Plugin for CakePHP 2.x
Version 1.2

Master: [https://gitlab.com/cakephp-2-x-plugins/zendframework](https://gitlab.com/cakephp-2-x-plugins/zendframework)

# General Information

The ZendFramework plugin allows for automagic loading of Zend Framework classes.

# Release Information

## Version 1.2
- Added ZendFramework 2.3.3
- Added support for selecting either framework.

## Version 1.1
- Updated to ZendFramework 1.12.8.
- General cleanup of code.

## Usage

To include the Zend Framework in your project, add the following code to your *app/Config/bootstrap.php* file: 

``` php
CakePlugin::load('ZendFramework', array('bootstrap' => true));
```

No other configuration is required to use the 1.x framework. You can now use the Zend Framework classes without any explicit include or require statements.

For more information on PHP's autoloading, see [Autoloading Classes](http://php.net/manual/en/language.oop5.autoload.php).

### Switching Versions

The default version is the 1.x branch.  If you wish to use the 2.x framework instead, add the following code **before** the call to *CakePlugin::load()*:
``` php
Configure::write('ZendFramework.version', 2);
```

## License
This plugin is released under the BSD License.  You can find a copy of this licenese in [LICENSE.md](LICENSE.md).

The Zend Framework is Copyright (c) 2005-2014, Zend Technologies USA, Inc. It is released under the Zend Framework license. You can find a copy of this license in [LICENSE.txt](Lib/1.12.8/LICENSE.txt).