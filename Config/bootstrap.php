<?php
require APP . 'Plugin' . DS . 'ZendFramework' . DS . 'Lib' . DS . 'ZendPlugin.php';

if (Configure::check('ZendFramework.version')) {
	// Only allow use of good versions.
	$version = Configure::read('ZendFramework.version');
	if ($version == 1) {
		ZendPlugin::$version = ZendPlugin::ZENDFRAMEWORK_1;
	} elseif ($verison == 2) {
		ZendPlugin::$version = ZendPlugin::ZENDFRAMEWORK_2;
	}
}

spl_autoload_register(array('ZendPlugin', 'load'));
// To keep Zend from fucking us
set_include_path(get_include_path() . PATH_SEPARATOR . APP . 'Plugin' . DS . 'ZendFramework' . DS . 'Vendor' . DS . ZendPlugin::$version);