<?php
/**
 * Autoloader for ZendFramework 1.x and 2.x.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 */
class ZendPlugin {
	// Currently supplied versions.
	const ZENDFRAMEWORK_2 = '2.3.3';
	const ZENDFRAMEWORK_1 = '1.12.8';

	// Default to the 1.x framework.
	public static $version = self::ZENDFRAMEWORK_1;
	public static $cache = false;

	/**
	 * Autoload function for Zend Framework.
	 *
	 * @param string $className
	 * @return boolean Success
	 * @since 1.0
	 */
	public static function load($className) {
		$folders = explode('_', $className);

		if (!empty($folders[0]) && $folders[0] == 'Zend') {
			// To avoid issues, return an absolute path
			$basePath = APP . 'Plugin' . DS . 'ZendFramework' . DS . 'Vendor' . DS .
					 ZendPlugin::$version;

			// Add the base path
			array_unshift($folders, $basePath);

			return include join(DS, $folders) . '.php';
		}

		return false;
	}

	/**
	 * Create a new Zend_Cache instance.
	 *
	 * @param string $frontend
	 * @param string $backend
	 * @param array $frontendOptions
	 * @param array $backendOptions
	 * @param string $customFrontendNaming
	 * @param string $customBackendNaming
	 * @param string $autoload
	 * @return Zend_Cache
	 * @since 1.0
	 */
	public static function cache($frontend, $backend, $frontendOptions = array(), $backendOptions = array(),
			$customFrontendNaming = false, $customBackendNaming = false, $autoload = false) {
		self::$cache = Zend_Cache::factory($frontend, $backend, $frontendOptions, $backendOptions,
				$customFrontendNaming, $customBackendNaming, $autoload);

		return self::$cache;
	}
}